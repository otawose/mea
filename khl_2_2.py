import sys
import itertools 

# assume the resistors form 2*2 matrix
def calc_end_resistance (r):
    r11 = r[0]
    r12 = r[1]
    r21 = r[2]
    r22 = r[3]

    z_a1 = 1.0 / (1.0/r11 + 1.0/(r21+r22+r12)) 
    z_b1 = 1.0 / (1.0/r12 + 1.0/(r22+r21+r11)) 
    z_a2 = 1.0 / (1.0/r21 + 1.0/(r11+r12+r22)) 
    z_b2 = 1.0 / (1.0/r22 + 1.0/(r12+r11+r21)) 

    return (z_a1, z_b1, z_a2, z_b2)

if '__main__' == __name__:

    # Possible parameters: any resistor could be either 100 or 500 Ohm
    v = [1., 500.] #500: live cells; 100: dead cells

    header = ['r11', 'r12', 'r21', 'r22', 'z_a1', 'z_b1', 'z_a2', 'z_b2']
    print '\t '.join(header) 

    for r in itertools.product(v, repeat=4):
        result = r + calc_end_resistance(r)
        print '\t '.join('{:0.0f}'.format(i) for i in result)

