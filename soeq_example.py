from numpy import array
from scipy.optimize import root

def function(x):
    f = array([x[0] + 2.* x[1] - 2, x[0]**2 + 4. * x[1]**2 - 4])
    df = array([[1., 2], [2 * x[0], 8 * x[1]]])
    return f, df

sol = root(function, array([1., 2]), method='hybr', jac = True)

print sol.x

def fun(x):
    return [x[0]  + 0.5 * (x[0] - x[1])**3 - 1.0, 0.5 * (x[1] - x[0])**3 + x[1]]

sol = root(fun, [0, 0], method='hybr')

print sol.x

