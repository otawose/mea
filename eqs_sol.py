'''
    File:   eqs_sol.py
    Author: dzhao@uw.edu
    Date:   9/23/2018
    Desc:   Solve the system of non-linear equations 
'''

import sys
from scipy.optimize import root
from eqs_func import func

# For unit test only
if __name__ == '__main__':

    if len(sys.argv) < 3:
        print "Please provide input N and Z's (" + sys.argv[0] + "  <N> <filename: Z's line-by-line>)"
        exit(0)

    n = int(sys.argv[1])
    with open(sys.argv[2]) as f:
        content = f.readlines()
    content = [x.strip() for x in content]
    if n**2 > len(content):
        print "Warning: " + sys.argv[0] + ": Mismatched N^2 and Z's", "N^2 =", n**2, "len(content) =", len(content), ", will use repeated values from the measurement file"

    # Should be very careful about how to choose initial values
    print("Start processing...")
    guess = []
    for i in range(n**2):
        guess += [float(content[i % len(content)])]
    for i in range(n**2, 2*n**3):
        guess += [.01]
        
    # Should also be very careful about which alg. to use for converging
    #md = 'hybr'
    md = 'lm'
    sol = root(func, guess, method=md)
    #print "Found a solution with U = " + "{0:0.20f}".format(sol.x[n**2 + (n-1)*2*n**2])
    print "Rs = " + str([float("{0:0.9f}".format(i)) for i in sol.x[range(n**2)]])
