#!/bin/bash

# File:     run_all_path.sh
# Author:   dzhao@uw.edu
# Date:     9/24/2018
# Desc:     Batch processing of all_path.py
# Use:      ./run_all_path.sh <start> <end>

if [ $# -lt 2 ];
then
    echo "Please provide start and end indices"
    exit 0
fi

args=("$@")
start=${args[0]}
end=${args[1]}
rm .tmpfile
for (( i=$start; i<=$end; i++ ))
do
    SECONDS=0
    python all_path.py $i > .tmpfile
    duration=$SECONDS
    echo "N = $i, Time: $duration seconds"
    date
done

