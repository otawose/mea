# Python program to print all paths from a source to destination.
#
#   Modified by:    donzhao@ucdavis.edu
#   Date:           9/24/2018

import sys
from collections import defaultdict

#This class represents a directed graph 
# using adjacency list representation
class Graph:

	def __init__(self,vertices):
		#No. of vertices
		self.V= vertices 
		
		# default dictionary to store graph
		self.graph = defaultdict(list) 

	# function to add an edge to graph
	def addEdge(self,u,v):
		self.graph[u].append(v)

	'''A recursive function to print all paths from 'u' to 'd'.
	visited[] keeps track of vertices in current path.
	path[] stores actual vertices and path_index is current
	index in path[]'''
	def printAllPathsUtil(self, u, d, visited, path):

		# Mark the current node as visited and store in path
		visited[u]= True
		path.append(u)

		# If current vertex is same as destination, then print
		# current path[]
		if u == d:
			print (path)
		else:
			# If current vertex is not destination
			#Recur for all the vertices adjacent to this vertex
			for i in self.graph[u]:
				if visited[i]==False:
					self.printAllPathsUtil(i, d, visited, path)
					
		# Remove current vertex from path[] and mark it as unvisited
		path.pop()
		visited[u]= False


	# Prints all paths from 's' to 'd'
	def printAllPaths(self,s, d):

		# Mark all the vertices as not visited
		visited =[False]*(self.V)

		# Create an array to store paths
		path = []

		# Call the recursive helper function to print all paths
		self.printAllPathsUtil(s, d, visited, path)

# Create a graph for an n by n matrix
def construct_graph(n):

    g = Graph(2*(n**2))

    for i in range(0,n):
        for j in range(0,n):

            # upper layer vs. lower layer
            g.addEdge((j)*2*n+2*i, (j)*2*n+2*i+1);
            g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i);                      
              
            if i == 0 and j == 0: #edges
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
                            
            elif i == 0 and j == n-1:
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
             
            elif i == 0 and j!=0 and j!=n-1:
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
                
            elif i == n-1 and j == 0: 
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);
              
            elif i == n-1 and j == n-1:
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);

            elif i == n-1 and j!=0 and j!=n-1:
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                    
            elif j==0 and i!=0 and i!=n-1:
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);

            elif j==n-1 and i!=0 and i!=n-1:
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
                           
            else: # not edges
                g.addEdge((j)*2*n+2*i, (j+1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i, (j-1)*2*n+2*i);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i-1);
                g.addEdge((j)*2*n+2*i+1, (j)*2*n+2*i+3);
    return g

if __name__ == '__main__':

    if len(sys.argv) < 2:
        print "Please provide matrix size N (" + sys.argv[0] + "  <N>)"
        exit(0)
    n = int(sys.argv[1])

    g = construct_graph(n)    

    for i in range(0, n**2):
            s = 2*i 
            d = 2*i+1
            print ("Following are all different paths from %d to %d :" %(s, d))
            g.printAllPaths(s, d)

# Paths for a single pair of end points:
#    if len(sys.argv) < 3:
#        print "Please provide source S and destination D (" + sys.argv[0] + "  <S> <D>)"
#        exit(0)
#    s = int(sys.argv[1])
#    d = int(sys.argv[2])
#    print ("Following are all different paths from %d to %d :" %(s, d))
#    g.printAllPaths(s, d)

